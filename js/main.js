/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/burger-btn.js":
/*!******************************!*\
  !*** ./src/js/burger-btn.js ***!
  \******************************/
/***/ (function() {

eval("const burgerBtn = document.querySelector('.header__burger-btn');\r\nconst list = document.querySelector('.nav');\r\nconst spans = document.querySelectorAll('.burger-line');\r\n\r\nburgerBtn.addEventListener('click', () => {\r\n\t\tlist.classList.toggle('hidden');\r\n\t\r\n\tif (burgerBtn.classList.contains('open')){\r\n\t\tburgerBtn.classList.remove('open');\r\n\t} else {\r\n\t\tburgerBtn.classList.add('open');\r\n\t}\r\n});\r\n\r\n\r\n\r\n\n\n//# sourceURL=webpack://scss---hw2-adv/./src/js/burger-btn.js?");

/***/ }),

/***/ "./src/js/main.js":
/*!************************!*\
  !*** ./src/js/main.js ***!
  \************************/
/***/ (function() {

"use strict";
eval("\r\n\r\n\r\n\n\n//# sourceURL=webpack://scss---hw2-adv/./src/js/main.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	__webpack_modules__["./src/js/burger-btn.js"]();
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/js/main.js"]();
/******/ 	
/******/ })()
;